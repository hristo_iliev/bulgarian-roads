# Републиканските пътища на България в GeoJSON формат #

Данните са взети от [www.openstreetmap.org](www.openstreetmap.org) към април 2015г.

Минифицираните версии са направени чрез [www.mapshaper.org](http://www.mapshaper.org) ([github](https://github.com/mbloch/mapshaper))

Лиценз: [Creative Commons Attribution-ShareAlike 2.0](https://creativecommons.org/licenses/by-sa/2.0/)
Лиценз на данните от OSM: [ODbL](http://wiki.openstreetmap.org/wiki/Open_Database_License)

Автор: Христо Илиев (hristo _ iliev <at> gmail)

*забележка:* някои от третокласните пътища липват в OSM. Информация може да намерите в wiki странацата им http://wiki.openstreetmap.org/wiki/Bg:WikiProject_Bulgaria/Пътна_мрежа